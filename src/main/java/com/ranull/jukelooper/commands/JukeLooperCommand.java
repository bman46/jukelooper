package com.ranull.jukelooper.commands;

import com.ranull.jukelooper.JukeLooper;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class JukeLooperCommand implements CommandExecutor {
    private JukeLooper plugin;

    public JukeLooperCommand(com.ranull.jukelooper.JukeLooper plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.5";
        String author = "Ranull";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.GOLD + "JukeLooper " + ChatColor.GRAY + "♫♪ "
                    + ChatColor.GRAY + "v" + version);
            sender.sendMessage(
                    ChatColor.GRAY + "/jukelooper " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET + " Plugin info");
            if (sender.hasPermission("jukelooper.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/jukelooper reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1 && args[0].equals("reload")) {
            if (!sender.hasPermission("jukelooper.reload")) {
                sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "JukeLooper" + ChatColor.DARK_GRAY + "]"
                        + ChatColor.RESET + " No Permission!");
                return true;
            }
            plugin.reloadConfig();
            sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "JukeLooper" + ChatColor.DARK_GRAY + "]"
                    + ChatColor.RESET + " Reloaded config file!");
        }
        return true;
    }
}
