package com.ranull.jukelooper.events;

import com.ranull.jukelooper.data.DataManager;
import com.ranull.jukelooper.looper.Looper;
import com.ranull.jukelooper.looper.LooperManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Jukebox;
import org.bukkit.block.data.Powerable;
import org.bukkit.block.data.type.Switch;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Events implements Listener {
    private LooperManager looperManager;
    private DataManager dataManager;

    public Events(LooperManager looperManager, DataManager dataManager) {
        this.looperManager = looperManager;
        this.dataManager = dataManager;
    }

    @EventHandler
    public void onButtonPress(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();

        if (block != null) {
            if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || !event.getPlayer().hasPermission("jukelooper.use")) {
                if (block.getType().equals(Material.STONE_BUTTON) || block.getType().equals(Material.OAK_BUTTON)
                        || block.getType().equals(Material.SPRUCE_BUTTON) || block.getType().equals(Material.BIRCH_BUTTON)
                        || block.getType().equals(Material.JUNGLE_BUTTON) || block.getType().equals(Material.ACACIA_BUTTON)
                        || block.getType().equals(Material.DARK_OAK_BUTTON)) {
                    Switch button = (Switch) block.getState().getBlockData();
                    BlockFace attached = button.getFacing().getOppositeFace();
                    if (block.getRelative(attached).getType().equals(Material.JUKEBOX)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onJukeboxInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (!player.hasPermission("jukelooper.use") || event.getClickedBlock() == null) {
            return;
        }

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)
                && event.getClickedBlock().getType().equals(Material.JUKEBOX)) {
            Jukebox jukebox = (Jukebox) event.getClickedBlock().getState();

            ItemStack itemInMainHand = player.getInventory().getItemInMainHand();

            Looper looper = looperManager.getLooper(jukebox.getLocation());

            if (jukebox.isPlaying()) {
                if (looper != null) {
                    looperManager.stopPlaying(looper);
                }
            } else {
                if (itemInMainHand.getType().isRecord()) {
                    if (looper == null) {
                        looper = new Looper(jukebox.getLocation(), itemInMainHand);
                    }

                    looperManager.updateLooper(looper);

                    player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));

                    event.setCancelled(true);
                } else {
                    if (looper != null) {
                        looperManager.stopPlaying(looper);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onJukeboxBreak(BlockBreakEvent event) {
        Looper looper = looperManager.getLooper(event.getBlock().getLocation());

        if (looper != null) {
            if (!event.getPlayer().hasPermission("jukelooper.remove") && dataManager.checkJukebox(looper)) {
                event.setCancelled(true);
                return;
            }

            dataManager.removeJukebox(looper);

            looperManager.stopPlaying(looper);
        }
    }

    @EventHandler
    public void onRedstone(BlockRedstoneEvent event) {
        Block block = event.getBlock();
        if (block.getType().equals(Material.STONE_BUTTON) || block.getType().equals(Material.OAK_BUTTON)
                || block.getType().equals(Material.SPRUCE_BUTTON) || block.getType().equals(Material.BIRCH_BUTTON)
                || block.getType().equals(Material.JUNGLE_BUTTON) || block.getType().equals(Material.ACACIA_BUTTON)
                || block.getType().equals(Material.DARK_OAK_BUTTON)) {
            Powerable button = (Powerable) block.getBlockData();
            for (BlockFace blockFace : LooperManager.blockFaces) {
                if (!button.isPowered() && block.getRelative(blockFace).getType().equals(Material.JUKEBOX)) {
                    Looper looper = looperManager.getLooper(block.getRelative(blockFace).getLocation());
                    if (looper != null) {
                        if (looperManager.findStorage(looper) != null) {
                            looperManager.nextDisc(looper);
                        }
                    }
                }
            }
        }
    }
}
