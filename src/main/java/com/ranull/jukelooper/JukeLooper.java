package com.ranull.jukelooper;

import com.ranull.jukelooper.looper.LooperManager;
import com.ranull.jukelooper.commands.JukeLooperCommand;
import com.ranull.jukelooper.data.DataManager;
import com.ranull.jukelooper.events.Events;
import org.bukkit.plugin.java.JavaPlugin;

public class JukeLooper extends JavaPlugin {
    private DataManager looperData;
    private LooperManager looperManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        looperData = new DataManager(this);
        looperManager = new LooperManager(this, looperData);

        this.getCommand("jukelooper").setExecutor(new JukeLooperCommand(this));

        this.getServer().getPluginManager().registerEvents(new Events(looperManager, looperData), this);
    }

    @Override
    public void onDisable() {
        looperManager.disableAllLoopers();
    }
}
