package com.ranull.jukelooper.looper;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class Looper {
    private ItemStack record;
    private Location location;
    private long startTime;
    private long duration;

    public Looper(Location location, ItemStack record) {
        this.location = location;
        this.record = record;
        this.startTime = System.currentTimeMillis();
    }

    public ItemStack getRecord() {
        return record;
    }

    public Location getLocation() {
        return location;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setRecord(ItemStack record) {
        this.record = record;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
