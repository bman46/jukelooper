package com.ranull.jukelooper.looper;

import com.ranull.jukelooper.JukeLooper;
import com.ranull.jukelooper.data.DataManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.Jukebox;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class LooperManager {
    private JukeLooper plugin;
    private DataManager dataManager;
    private Map<Location, Looper> looperMap = new HashMap<>();

    public LooperManager(JukeLooper plugin, DataManager dataManager) {
        this.plugin = plugin;
        this.dataManager = dataManager;

        enableAllLoopers();

        secondTimer();
    }

    public void secondTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Map.Entry<Location, Looper> entry : looperMap.entrySet()) {
                    Looper looper = entry.getValue();

                    long playTime = System.currentTimeMillis() - looper.getStartTime();
                    if (playTime >= looper.getDuration()) {
                        nextDisc(looper);
                    }
                }
            }
        }.runTaskTimer(plugin, 0L, 5L);
    }

    public void enableAllLoopers() {
        FileConfiguration looperData = dataManager.getLooperData();

        for (String world : looperData.getKeys(false)) {
            for (String box : looperData.getConfigurationSection(world).getKeys(false)) {
                String[] cords = box.split("_");

                int x = Integer.valueOf(cords[0]);
                int y = Integer.valueOf(cords[1]);
                int z = Integer.valueOf(cords[2]);

                Location location = new Location(plugin.getServer().getWorld(world), x, y, z);

                if (location.getBlock().getType().equals(Material.JUKEBOX)) {
                    Jukebox jukebox = (Jukebox) location.getBlock().getState();
                    Looper looper = new Looper(location, jukebox.getRecord());

                    updateLooper(looper);
                }
            }
        }
    }

    public void disableAllLoopers() {
        for (Map.Entry<Location, Looper> entry : looperMap.entrySet()) {
            stopPlaying(entry.getValue());
        }
    }

    public void updateLooper(Looper looper) {
        Jukebox jukebox = getLooperJukebox(looper);

        if (jukebox != null) {
            looper.setStartTime(System.currentTimeMillis());
            looper.setDuration(getDuration(looper));

            int trackType = trackType(looper);

            if (trackType == 1) {
                jukebox.setRecord(jukebox.getRecord());
                jukebox.setPlaying(looper.getRecord().getType());
                jukebox.update(true);
            } else if (trackType == 2) {
                // TODO Custom records
            }

            looperMap.put(looper.getLocation(), looper);
            dataManager.saveJukebox(looper);
        }
    }

    public Looper getLooper(Location location) {
        return looperMap.get(location);
    }

    public Jukebox getLooperJukebox(Looper looper) {
        if (looper.getLocation().getBlock().getType().equals(Material.JUKEBOX)) {
            return (Jukebox) looper.getLocation().getBlock().getState();
        }
        
        return null;
    }

    public long getDuration(Looper looper) {
        if (plugin.getConfig().isSet("recordDurations." + looper.getRecord().getType().toString())) {
            return plugin.getConfig().getLong("recordDurations." + looper.getRecord().getType().toString()) * 1000;
        } else if (plugin.getConfig().isSet("customDurations." + looper.getRecord().getType().toString())) {
            return plugin.getConfig().getLong("recordDurations." + looper.getRecord().getType().toString() + ".duration") * 1000;
        } else {
            return 0;
        }
    }

    public int trackType(Looper looper) {
        // TODO Custom tracks
        return 1;
    }

    public void stopPlaying(Looper looper) {
        Jukebox jukebox = getLooperJukebox(looper);

        if (jukebox != null) {
            jukebox.setRecord(looper.getRecord());
            jukebox.setPlaying(null);
            jukebox.update();

            looperMap.remove(looper.getLocation());
        }
    }

    public void nextDisc(Looper looper) {
        Jukebox jukebox = getLooperJukebox(looper);

        if (jukebox != null) {
            Block storage = findStorage(looper);

            if (storage != null) {
                putItemInStorage(jukebox.getRecord(), storage.getLocation());

                ItemStack record = takeItemFromStorage(storage.getLocation());

                if (record != null) {
                    looper.setRecord(record);

                    updateLooper(looper);
                }
            }
        }
    }

    public Boolean putItemInStorage(ItemStack item, Location storage) {
        if (storage != null && storage.getBlock().getType().equals(Material.CHEST)) {
            Chest chest = (Chest) storage.getBlock().getState();

            chest.getBlockInventory().addItem(item);

            return true;
        }

        return false;
    }

    public ItemStack takeItemFromStorage(Location storage) {
        if (!storage.getBlock().getType().equals(Material.CHEST)) {
            return null;
        }

        Chest chest = (Chest) storage.getBlock().getState();

        List<ItemStack> recordList = new ArrayList<>();

        for (ItemStack item : chest.getBlockInventory().getContents()) {
            if (item != null && item.getType().isRecord()) {
                recordList.add(item);
            }
        }

        if (!recordList.isEmpty()) {
            Collections.shuffle(recordList);

            chest.getBlockInventory().removeItem(recordList.get(0));

            return recordList.get(0);
        }

        return null;
    }

    public Block findStorage(Looper looper) {
        Block block = looper.getLocation().getBlock();
        
        for(int i = blockFaces.length - 1; i >= 0; i--){
            if(block.getRelative(blockFaces[i]).getType().equals(Material.CHEST)){
                return block.getRelative(blockFaces[i]);
            }
        }

        return null;
    }

    public static BlockFace[] blockFaces = new BlockFace[]{BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH,
            BlockFace.SOUTH, BlockFace.DOWN, BlockFace.UP};
}
