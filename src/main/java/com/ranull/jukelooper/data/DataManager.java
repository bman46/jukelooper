package com.ranull.jukelooper.data;

import com.ranull.jukelooper.JukeLooper;
import com.ranull.jukelooper.looper.Looper;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class DataManager {
    private JukeLooper plugin;
    private FileConfiguration looperData;
    private File dataFile;

    public DataManager(JukeLooper plugin) {
        this.plugin = plugin;
        createDataFile();
    }

    public FileConfiguration getLooperData() {
        return this.looperData;
    }

    public void saveJukebox(Looper looper) {
        Location location = looper.getLocation();

        String world = location.getWorld().getName();

        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();

        looperData.set(world + "." + x + "_" + y + "_" + z, looper.getRecord().getType().toString());
        
        saveData();
    }

    public boolean checkJukebox(Looper looper) {
        String world = looper.getLocation().getWorld().getName();

        int x = looper.getLocation().getBlockX();
        int y = looper.getLocation().getBlockY();
        int z = looper.getLocation().getBlockZ();

        return (looperData.getString(world + "." + x + "_" + y + "_" + z) != null);
    }

    public void removeJukebox(Looper looper) {
        Location location = looper.getLocation();

        String world = location.getWorld().getName();

        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();

        looperData.set(world + "." + x + "_" + y + "_" + z, null);

        saveData();
    }

    public void saveData() {
        try {
            looperData.save(dataFile);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void createDataFile() {
        dataFile = new File(plugin.getDataFolder(), "data.yml");

        if (!dataFile.exists()) {
            dataFile.getParentFile().mkdirs();

            try {
                dataFile.createNewFile();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        looperData = new YamlConfiguration();

        try {
            looperData.load(dataFile);
        } catch (IOException | InvalidConfigurationException exception) {
            exception.printStackTrace();
        }
    }
}
